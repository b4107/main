$(document).ready(function() {
  $(".dropdown-button").dropdown();  
  // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
  $('.modal-trigger').leanModal();

  // attaches chosen Manage Option to appropriate manage div
  $('.dropdown-content li').on('click',function(index){

    // clear the welcome text, once something is selected
    if($("#welcomeText_intrainer").css('display') === 'block') {
        $("#welcomeText_intrainer").css('display', 'none');
    }

    // get id of clicked element
    var elementId = $(this).attr('id'); 

    // remove "tab_" from the string (elementId), append "Text" to it
    elementId = elementId.substring(4, elementId.length);
    elementId = elementId + "Text";

    // print new elementId for debugging purposes
    console.log(elementId);
  
    // clear other displayed tab(s)
    $( "#login_wrapper div" ).each(function( index ) {
      if($(this).hasClass("activeTab")) {
         $(this).removeClass("activeTab").addClass("hiddenTab");

      }
    
    });
    
    // now that every tab is cleared, display the correct one
    $("#" + elementId).removeClass("hiddenTab").addClass("activeTab");

    /*
    Execution example:
    * elementId = tab_editEquipments
    * convert elementId into editEquipmentsText
    * now we have appropriate elementID
    * display the div with the newly created elementID
    */
  
  });
  

});